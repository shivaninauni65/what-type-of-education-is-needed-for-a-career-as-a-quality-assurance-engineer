# What type of education is needed for a career as a quality assurance engineer

[QA engineer training](https://testpro.io/bootcamp/qa-engineer/) typically start as software developers, computer scientists or software engineers. They later make the transition to QA engineering.

It is the first thing to acquire the fundamentals in programming as well as software creation. Learn these skills by completing a university degree, online learning or through working experiences. It is important to select the right training or environment that teaches not just coding as well as the most effective methods of software development life cycle.

Most employers do not recruit QA technicians based upon their qualifications however, they do recruit candidates from their developer pool. Therefore, the next step is to get experience as a software developer part of a team. When employers are hiring QA technicians, they search for developers who have the skills of communication and strategic that we mentioned earlier. They might then provide further training and the ability to specialize.

